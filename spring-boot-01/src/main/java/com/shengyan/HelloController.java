package com.shengyan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName HelloController
 * @Description
 * @Author linshengyan
 * @Date 2022/4/11 9:23 下午
 */
@Controller
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    HelloService helloService;

    @RequestMapping(value = "/hello",produces="application/json;charset=UTF-8")
    @ResponseBody
    public String hello() {
        return helloService.sayHello("林生燕");
    }
}
