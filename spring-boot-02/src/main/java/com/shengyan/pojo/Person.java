package com.shengyan.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @PropertySource加载指定的配置文件
 * @author create by lsy on 2022/1/11 7:16 下午
 */
@Data
@Component
@PropertySource(value = "classpath:person.properties")
public class Person {
    @Value("${name}")
    private String name;
}
