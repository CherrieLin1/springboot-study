package com.shengyan.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author create by lsy on 2022/1/11 7:22 下午
 */
@Component
@Data
public class Dog {
    private String name;
    private int age;
}
