package com.shengyan.pojo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Email;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ConfigurationProperties指定从全局配置文件中加载
 * @author create by lsy on 2022/1/6 6:25 下午
 */
@Component
@ConfigurationProperties(prefix = "user")
@Data
@Validated
public class User {
    private String userName;
    private int age;
    @Email(message = "邮箱格式错误")
    private String email;
    private String address;
    private int[] array;
    private List<String> list;
    private Map<String,Object> maps;
    private Date birth;
    private Dog dog;
}
