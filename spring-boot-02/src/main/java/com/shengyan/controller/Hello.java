package com.shengyan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author create by lsy on 2022/1/5 1:59 下午
 */
@Controller
@RequestMapping("/hello")
public class Hello {

    @RequestMapping("/hello")
    @ResponseBody
    public String sayHello(){
        return "hello,springBoot!";

    }
}
