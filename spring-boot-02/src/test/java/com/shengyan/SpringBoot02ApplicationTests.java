package com.shengyan;

import com.shengyan.pojo.Person;
import com.shengyan.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBoot02ApplicationTests {

    @Autowired
    User user;

    @Autowired
    Person person;

    @Test
    void contextLoads() {
        System.out.println("user = " + user);
        System.out.println("person = " + person);
    }

}
