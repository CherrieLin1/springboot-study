package com.shengyan.springbootweb;

import com.shengyan.springbootweb.mapper.DepartmentMapper;
import com.shengyan.springbootweb.mapper.EmployeeMapper;
import com.shengyan.springbootweb.pojo.Department;
import com.shengyan.springbootweb.pojo.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpringBootWebApplicationTests {
    @Autowired
    EmployeeMapper employeeMapper;

    @Autowired
    DepartmentMapper departmentMapper;


    @Test
    void contextLoads() {
    }


    @Test
    public void testGetDepartList() {
        Department department = departmentMapper.getDepartment(101);
        if (null != department){
            System.out.println("department = " + department);
        }
    }

    @Test
    public void testGetEmployee() {
        List<Employee> employees = employeeMapper.getEmployees();
    }
}
