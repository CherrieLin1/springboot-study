package com.shengyan.springbootweb.mapper;

import com.shengyan.springbootweb.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @ClassName HelloController
 * @Description
 * @Author linshengyan
 * @Date 2022/4/11 9:23 下午
 * @Mapper : 表示本类是一个 MyBatis 的 Mapper
 */
@Mapper
@Repository
public interface EmployeeMapper {


    /**
     * 获取所有员工信息
     *
     * @return
     */
    List<Employee> getEmployees();


    /**
     * 新增一个员工
     *
     * @param employee
     * @return
     */
    int save(Employee employee);

    /**
     * 修改员工信息
     *
     * @param employee
     * @return
     */
    int update(Employee employee);


    /**
     * 通过id获得员工信息
     *
     * @param id
     * @return
     */
    Employee get(Integer id);

    /**
     * 通过id删除员工
     *
     * @param id
     * @return
     */
    int delete(Integer id);

    /**
     * 根据用户名密码查询一个用户
     *
     * @param loginName
     * @param password
     * @return
     */
    Employee queryByIdAndPwd(String loginName, String password);

}
