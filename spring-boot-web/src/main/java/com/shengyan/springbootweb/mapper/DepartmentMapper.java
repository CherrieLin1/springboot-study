package com.shengyan.springbootweb.mapper;

import com.shengyan.springbootweb.pojo.Department;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ClassName HelloController
 * @Description
 * @Author linshengyan
 * @Date 2022/4/11 9:23 下午
 *
 * @Mapper : 表示本类是一个 MyBatis 的 Mapper
 */
@Mapper
@Repository
public interface DepartmentMapper {

    /**
     * 获取所有部门信息
     * @return
     */
    List<Department> getDepartments();


    /**
     * 通过id获得部门
     * @param id
     * @return
     */
    Department getDepartment(Integer id);

}
