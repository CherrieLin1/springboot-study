package com.shengyan.springbootweb.controller;

import com.shengyan.springbootweb.mapper.EmployeeMapper;
import com.shengyan.springbootweb.pojo.Employee;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    EmployeeMapper employeeMapper;

    @PostMapping("/user/login")
    public String login(@RequestParam("username") String username,
                        @RequestParam("password") String password,
                        Model model, HttpSession session) {

        Employee employee = employeeMapper.queryByIdAndPwd(username, password);
        if (null == employee) {
            //登录失败！存放错误信息
            model.addAttribute("msg", "用户名或者密码错误");
            return "index";
        } else {
            //登录成功！将用户信息放入session
            session.setAttribute("loginUser", username);
            //登录成功！防止表单重复提交，我们重定向
            return "redirect:/main.html";
        }


    }

    @GetMapping("/user/loginOut")
    public String loginOut(HttpSession session) {
        //session失效
        session.invalidate();
        return "redirect:/index.html";
    }

}