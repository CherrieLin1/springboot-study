package com.shengyan.springbootweb.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @ClassName 员工表
 * @Description
 * @Author linshengyan
 * @Date 2022/4/11 9:23 下午
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    /**
     * id
     */
    private Integer id;

    /**
     * lastName
     */
    private String lastName;

    /**
     * loginName
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * email
     */
    private String email;

    /**
     * 1 male, 0 female
     */
    private Integer gender;

    /**
     * 部门id
     */
    private Integer department;

    /**
     * 生日
     */
    private Date birth;

    /**
     * 部门
     */
    private Department eDepartment;
}
