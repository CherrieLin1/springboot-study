package com.shengyan.springbootweb.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @ClassName LoginHandlerInterceptor
 * @Description 登陆拦截器
 * @Author linshengyan
 * @Date 2022/4/21 4:19 下午
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 获取LoginUser信息进行判断
        Object user = request.getSession().getAttribute("loginUser");
        //未登录，返回登录页面
        if (user == null) {
            request.setAttribute("msg", "没有权限，请先登录");
            request.getRequestDispatcher("/index.html").forward(request, response);
            return false;
        } else {
            //登录，放行
            return true;
        }
    }
}
