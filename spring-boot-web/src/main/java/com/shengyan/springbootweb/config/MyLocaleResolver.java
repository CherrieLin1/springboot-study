package com.shengyan.springbootweb.config;


import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @ClassName MyLocaleResolver
 * @Description 区域解析器
 * @Author linshengyan
 * @Date 2022/4/21 4:21 下午
 */
public class MyLocaleResolver implements LocaleResolver {


    /**
     * 解析请求中的设置，如果没有设置，默认为null
     *
     * @param request
     * @return
     */
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        //从请求中获取语言
        String language = request.getParameter("language");
        System.out.println("language = " + language);

        //获取默认的地区解析器
        Locale locale = Locale.getDefault();

        //请求中设置的语言不为null
        if (StringUtils.isNotEmpty(language)) {
            //en_us   zh_cn
            String[] str = language.split("_");
            locale = new Locale(str[0], str[1]);
            return locale;

        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {

    }
}
