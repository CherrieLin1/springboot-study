package com.shengyan.springbootweb.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @ClassName MyMvcConfig
 * @Description mvc扩展文件，保留springmvc的配置，并且希望添加其他MVC配置（拦截器、格式化程序、视图控制器和其他功能），则可以添加自己
 * @Author linshengyan
 * @Date 2022/4/21 4:08 下午
 */
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    /**
     * 实现自定义的试图控制器，这在不需要自定义控制器逻辑的情况下非常有用
     * 例如，呈现主页、执行简单的站点URL重定向、返回包含HTML内容的404状态、返回不包含任何内容的204状态等等。
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/login").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        registry.addViewController("/main.html").setViewName("dashboard");
    }


    /**
     * 注册拦截器，及拦截请求和要剔除哪些请求!
     * 我们还需要过滤静态资源文件，否则样式显示不出来
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new LoginHandlerInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/index.html","/","/user/login","/asserts/**","/favicon.ico","/login");
    }

    /**
     * 注册地区解析器
     * 注意⚠️这里一定要注意，解析器的名字是localeResolver，其他的都不会生效
     * @return
     */
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }

}
