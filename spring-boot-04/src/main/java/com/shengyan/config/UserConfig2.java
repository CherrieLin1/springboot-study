package com.shengyan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserConfig2
 * @Description
 * 配置方式二：使用@ConfigurationProperties(prefix ="xxxx") + @Component方式
 * 注意：此种方式配置的时候，需要加注解将该类扫描成一个spring bean
 * @Author linshengyan
 * @Date 2022/4/21 9:50 上午
 */
@Component
@ConfigurationProperties(prefix = "user2.test")
@Data
public class UserConfig2 {
    private String userName;

    private String nickName;

    private String desc;
}
