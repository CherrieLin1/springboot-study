package com.shengyan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName UserConfig3
 * @Description
 * 配置方式四：使用@ConfigurationProperties(prefix ="xxxx") + @Bean方式
 * @Author linshengyan
 * @Date 2022/4/21 9:50 上午
 */
@ConfigurationProperties("user4.test")
@Data
public class UserConfig4 {
    private String userName;

    private String nickName;

    private String desc;
}
