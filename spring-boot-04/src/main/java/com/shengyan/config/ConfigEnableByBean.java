package com.shengyan.config;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @ClassName ConfigEnableByBean
 * @Description  此处@Configuration 和@Component效果一样
 * @Author linshengyan
 * @Date 2022/4/21 10:24 上午
 */
//@Configuration
@Component
@Data
public class ConfigEnableByBean {

    @Bean
    public UserConfig4 userConfig4() {
        return new UserConfig4();
    }
}
