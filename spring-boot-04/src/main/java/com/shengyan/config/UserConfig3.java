package com.shengyan.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName UserConfig3
 * @Description
 * 配置方式三：使用@ConfigurationProperties(prefix ="xxxx") + @EnableConfigurationProperties方式
 * @Author linshengyan
 * @Date 2022/4/21 9:50 上午
 */
@ConfigurationProperties("user3.test")
@Data
public class UserConfig3 {
    private String userName;

    private String nickName;

    private String desc;
}
