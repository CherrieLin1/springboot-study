package com.shengyan.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @ClassName ConfigEnable
 * @Description  该类作用是将UserConfig3加载为一个配置类并更够被Spring扫描到
 * @Author linshengyan
 * @Date 2022/4/21 10:14 上午
 */
@Component
@EnableConfigurationProperties(UserConfig3.class)
public class ConfigEnableConfig {
}
