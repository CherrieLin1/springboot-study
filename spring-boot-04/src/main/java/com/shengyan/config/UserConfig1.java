package com.shengyan.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @ClassName UserConfig1
 * @Description
 * 配置方式一：使用@Configuration + @Value方式
 * 此处的@Configuration可以替换成@Component，效果一样
 * @Author linshengyan
 * @Date 2022/4/21 9:49 上午
 */
@Data
@Configuration
@Component
public class UserConfig1 {
    @Value("${user1.test.userName}")
    private String userName;

    @Value("${user1.test.nickname}")
    private String nickName;

    @Value("${user1.test.desc}")
    private String desc;

}
