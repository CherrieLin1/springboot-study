package com.shengyan;

import com.shengyan.config.UserConfig1;
import com.shengyan.config.UserConfig2;
import com.shengyan.config.UserConfig3;
import com.shengyan.config.UserConfig4;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBoot04ApplicationTests {
    @Autowired
    UserConfig1 userConfig1;

    @Autowired
    UserConfig2 userConfig2;

    @Autowired
    UserConfig3 userConfig3;

    @Autowired
    UserConfig4 userConfig4;

    @Test
    void contextLoads() {
    }

    @Test
    public void testUserConfig1() {
        System.out.println("userConfig1.getUserName() = " + userConfig1.getUserName());
        System.out.println("userConfig1.getNickName() = " + userConfig1.getNickName());
        System.out.println("userConfig1.getDesc() = " + userConfig1.getDesc());

    }


    @Test
    public void testUserConfig2() {
        System.out.println("userConfig2.getUserName() = " + userConfig2.getUserName());
        System.out.println("userConfig2.getNickName() = " + userConfig2.getNickName());
        System.out.println("userConfig2.getDesc() = " + userConfig2.getDesc());

    }

    @Test
    public void testUserConfig3() {
        System.out.println("userConfig3.getUserName() = " + userConfig3.getUserName());
        System.out.println("userConfig3.getNickName() = " + userConfig3.getNickName());
        System.out.println("userConfig3.getDesc() = " + userConfig3.getDesc());

    }

    @Test
    public void testUserConfig4() {
        System.out.println("userConfig4.getUserName() = " + userConfig4.getUserName());
        System.out.println("userConfig4.getNickName() = " + userConfig4.getNickName());
        System.out.println("userConfig4.getDesc() = " + userConfig4.getDesc());
    }

}
