package com.shengyan;

import com.shengyan.pojo.Dog;
import com.shengyan.pojo.Person;
import com.shengyan.pojo.User;
import com.shengyan.utils.ValidateUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.validation.groups.Default;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.misc.Version.print;


/**
 * jsr303校验测试
 * https://blog.csdn.net/qq_27680317/article/details/79970590
 */
@SpringBootTest
class SpringBoot03ApplicationTests {

    @Autowired
    User user = new User();


    /**
     * 利用spring的自动装配，yaml中配置了user的信息，并且user对应添加了对应的注解，这时候必须到导入两个包
     * 1.jsr303的包，该包的作用是添加对应的规则，例如@NotNull  @Email
     * 2.hibernate-api该包的作用是使@Validated注解生效，该注解属于spring的，但是没有实现
     *
     * @Validated 会按照对象上配置的规则校验参数
     */
    @Test
    public void contextLoads() {
        System.out.println("user = " + user);
    }


    /**
     * 分组校验
     * 前提：字段指定了分组；字段没有指定分组
     * 校验：
     * 1.指定了组校验，会校验对应的分组信息，没有指定分组的也会校验
     * 2.不指定分组信息，只会校验无分组信息的属性值
     *
     */
    @Test
    public void testByGroup(){
        Person person  = new Person();
        person.setName("hj");
        person.setSex("男");
        person.setAge(12);
        person.setPhone("rtio");
        Dog dog = new Dog();
        dog.setName("可爱的狗狗");
        person.setDog(dog);

        System.out.println("person = " + person);
        List<String> list = ValidateUtils.validateEntity(person, Default.class);
        System.out.println("list = " + list);


    }


}
