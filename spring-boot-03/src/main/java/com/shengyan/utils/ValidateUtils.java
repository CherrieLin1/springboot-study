package com.shengyan.utils;

import org.apache.commons.collections.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author create by lsy on 2022/1/13 2:44 下午
 */
public class ValidateUtils {
    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();


    /**
     * 校验默认信息
     * @param data
     * @param <T>
     */
    public static <T> void validate(T data) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(data);
        if (CollectionUtils.isEmpty(constraintViolations)) {
            return;
        }
        String errorMsg = constraintViolations.iterator().next().getMessage();
        throw new ValidationException(errorMsg);
    }


    /**
     * 按分组校验
     * 只要发现一个不正常的就抛出异常信息，业务方捕获异常信息，可以手动捕获，也可以用spring切面的方式捕获
     * @param data
     * @param groupClazz
     * @param <T>
     */
    public static <T> void validateByGroup(T data, Class<?> groupClazz) {
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(data, groupClazz);
        if (CollectionUtils.isEmpty(constraintViolations)) {
            return;
        }
        String errorMsg = constraintViolations.iterator().next().getMessage();
        throw new ValidationException(errorMsg);
    }


    /**
     * 将所有不满足的信息透出
     * @param object
     * @param groups
     * @return
     */
    public static List<String> validateEntity(Object object, Class<?>... groups) {
        Set<ConstraintViolation<Object>> validate = validator.validate(object, groups);
//		ConstraintViolation<Object> constraint = validate.iterator().next();
        List<String> list = new ArrayList<>();
        for(ConstraintViolation<Object> aa :validate) {
            list.add(aa.getMessage());
        }
        return list;
    }

}
