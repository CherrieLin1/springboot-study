package com.shengyan.pojo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author create by lsy on 2022/1/13 2:39 下午
 */
@Data
public class UserAccount {
    @NotNull
    @Size(min = 4, max = 15)
    private String password;

    @NotBlank
    private String name;
}
