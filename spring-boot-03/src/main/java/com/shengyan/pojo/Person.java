package com.shengyan.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

/**
 * @author create by lsy on 2022/1/12 7:48 下午
 */
@Data
@Component
public class Person {
    @NotNull(message = "姓名不可以为null")
    private String name;

    @NotNull(message = "性别不可以为null", groups = BasicInfo.class)
    private String sex;

    @NotNull(message = "手机号码不可以为空", groups = ExtraInfo.class)
    private String phone;

    @NotNull(message = "家庭住址不可以为null", groups = ExtraInfo.class)
    private String address;


    @Min(value = 18, message = "未成名人不准进入", groups = ExtraInfo.class)
    private int age;

    /**
     * @Valid会校验对象里面的参数不为null
     */
    @NotNull(message = "宠物不可以为null")
    @Valid
    private Dog dog;

    public interface BasicInfo extends Default {
    }

    public interface ExtraInfo extends Default{
    }
}


