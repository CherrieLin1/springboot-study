package com.shengyan.pojo;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author create by lsy on 2022/1/13 4:36 下午
 */
@Data
public class Dog {
    @NotNull(message = "狗的名字不可以为null")
    private String name;

    @NotNull(message = "狗的年龄不可以为null")
    @Min(value = 1,message = "年龄不能小于1")
    private int age;

}
