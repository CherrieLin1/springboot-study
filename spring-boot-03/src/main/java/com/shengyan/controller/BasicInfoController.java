package com.shengyan.controller;

import com.shengyan.pojo.UserAccount;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author create by lsy on 2022/1/13 2:41 下午
 */
public class BasicInfoController {

    @RequestMapping(value = "/saveBasicInfo", method = RequestMethod.POST)
    public String saveBasicInfo(@Valid @ModelAttribute("useraccount") UserAccount useraccount, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        return "success";
    }
}
