package com.shengyan;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName HelloProperties
 * @Description
 * @Author linshengyan
 * @Date 2022/4/11 9:07 下午
 */
@ConfigurationProperties(prefix = "kuangsheng")
public class HelloProperties {
    private String prefix;

    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
